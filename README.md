The Country Club at DC Ranch is North Scottsdale's premier private golf and country club, celebrating friends and families coming together to enjoy the best of times in a truly distinctive setting, including golf on the club's classic course redesigned by Tom Lehman and John Fought.

Address: 9290 E Thompson Peak Parkway, Scottsdale, AZ 85255, USA

Phone: 480-342-7200
